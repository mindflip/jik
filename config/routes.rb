Rails.application.routes.draw do
  devise_for :users, controllers: {
  	registrations: 'users/registrations'
	}
  root 'jung#home'
  match ":controller(/:action(/:id))", :via => [:get, :post]
end
