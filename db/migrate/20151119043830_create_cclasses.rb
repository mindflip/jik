class CreateCclasses < ActiveRecord::Migration
  def change
    create_table :cclasses do |t|
    	t.integer :user_id
    	t.integer :cur_adult_statndard_id
    	t.integer :prev_adult_statndard_id

      t.timestamps null: false
    end
  end
end
