class CreateCurAdultStandards < ActiveRecord::Migration
  def change
    create_table :cur_adult_standards do |t|
    	t.string :title
    	t.text :content
      t.timestamps null: false
    end
  end
end
