class CreatePrevAdultStandards < ActiveRecord::Migration
  def change
    create_table :prev_adult_standards do |t|
    	t.string :title
    	t.text :content
      t.timestamps null: false
    end
  end
end
