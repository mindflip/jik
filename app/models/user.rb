class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :cclasses
  has_many :cur_adult_standard, :through => :cclasses
  has_many :prev_adult_standard, :through => :cclasses
end
